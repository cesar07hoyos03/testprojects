package com.example.springaop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController(value = "person")
public class PersonController {

    @Autowired
    IPersonService personService;

    @PostMapping
    public ResponseEntity<String> addPerson(@RequestBody Person p) {
        log.info(String.format("Saving person: %s", p));
        this.personService.addPerson(p);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons() {
        log.info("Getting all Persons");
        return ResponseEntity.ok(this.personService.getAllPersons());
    }
}
