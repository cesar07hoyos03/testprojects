package com.example.springaop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService implements IPersonService {

    @Autowired
    PersonRepository personRepository;

    @Override
    @LogExecutionTime
    public void addPerson(Person p) {
        this.personRepository.save(p);
    }

    @Override
    public List<Person> getAllPersons(){
        return this.personRepository.findAll();
    }
}
