package com.example.springaop;

import java.util.List;

public interface IPersonService {

    void addPerson(Person p);

    List<Person> getAllPersons();

}
