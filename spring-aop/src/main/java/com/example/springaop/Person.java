package com.example.springaop;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person", schema = "test")
public class Person {

    @Id
    private String id;

    private String name;

}
