package com.example.springeureka;

import com.example.springeureka.filters.AddResponseHeaderFilter;
import com.example.springeureka.filters.AuthorizationPreFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@EnableZuulProxy
@EnableEurekaServer
@SpringBootApplication
public class SpringEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringEurekaApplication.class, args);
    }

    @Bean
    public AuthorizationPreFilter preFilter() {
        return new AuthorizationPreFilter();
    }

    @Bean
    public AddResponseHeaderFilter postFilter() {
        return new AddResponseHeaderFilter();
    }


}
