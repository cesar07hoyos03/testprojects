package com.example.springeureka.util;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Slf4j
public class EurekaUtilities {

    public static void printHeaders(HttpServletRequest request, Enumeration<String> headerNames) {
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            log.info("** Header Name: " + headerName);
            String headerValue = request.getHeader(headerName);
            log.info("++ Header Value: " + headerValue);
        }
    }
}
