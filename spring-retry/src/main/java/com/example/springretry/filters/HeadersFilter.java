package com.example.springretry.filters;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@Component
@Order(1)
@Slf4j
public class HeadersFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        printHeaders(req, req.getHeaderNames());
        chain.doFilter(request, response);
    }


    static void printHeaders(HttpServletRequest request, Enumeration<String> headerNames) {
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if (headerName.equalsIgnoreCase("authorization")) {
                log.info("** Header Name: " + headerName);
                String headerValue = request.getHeader(headerName);
                log.info("++ Header Value: " + headerValue);
            }else{
                log.info("** Authorization Header is not present!");
            }
        }
    }
}
