package com.example.springretry.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {

    @Retryable(
            value = {NumberFormatException.class},
            maxAttempts = 4,
            backoff = @Backoff(
                    delay = 1000,
                    multiplier = 2
            )
    )
    @GetMapping
    public String hello(@RequestParam(value = "name") String name) {
        log.info("calling hello Controller");
        Integer.parseInt("");
        return "hello " + name + "!";
    }

    @Recover
    public String recover(NumberFormatException ex) {
        log.info("Trying to recover NumberFormatException..");
        throw ex;
    }


}

