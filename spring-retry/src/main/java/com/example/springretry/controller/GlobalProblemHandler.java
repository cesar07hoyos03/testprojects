package com.example.springretry.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalProblemHandler {

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<String> numberFormatException(final NumberFormatException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Oops!");
    }

}
