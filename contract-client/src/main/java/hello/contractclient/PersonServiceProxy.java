package hello.contractclient;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "person-service", fallback = PersonServiceFallback.class)
@RibbonClient(name = "person-service", configuration = PersonServiceConfiguration.class)
public interface PersonServiceProxy {

    @LoadBalanced
    @RequestMapping("/person/{id}")
    Person findById(@PathVariable(value = "id") Long id);
}
