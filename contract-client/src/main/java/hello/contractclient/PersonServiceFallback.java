package hello.contractclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PersonServiceFallback implements PersonServiceProxy {

    @Override
    public Person findById(Long id) {

        log.error("Person service is not available");

        return new Person();
    }
}
