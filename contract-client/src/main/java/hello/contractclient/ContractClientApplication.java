package hello.contractclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableFeignClients
@SpringBootApplication
@EnableCircuitBreaker
public class ContractClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContractClientApplication.class, args);
    }
}

@RestController
class MessageRestController {

    @Autowired
    PersonServiceProxy proxy;


    @RequestMapping("/message/{personId}")
    String getMessage(@PathVariable("personId") Long personId) {
        Person person = this.proxy.findById(personId);
        return "Hello " + person.getName();
    }

}