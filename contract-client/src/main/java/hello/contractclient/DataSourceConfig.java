package hello.contractclient;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "hello.contractclient")
public class DataSourceConfig {

    /**
     * username datasource property
     */
    @Value("${spring.datasource.username}")
    private String user;

    /**
     * username password property
     */
    @Value("${spring.datasource.password}")
    private String password;

    /**
     * dataSourceUrl hikari property
     */
    @Value("${spring.datasource.url}")
    private String dataSourceUrl;

    /**
     * poolName hikari property
     */
    @Value("${spring.datasource.dataSourceClassName}")
    private String dataSourceClassName;

    /**
     * poolName hikari property
     */
    @Value("${spring.datasource.poolName}")
    private String poolName;

    /**
     * connectionTimeout hikari property
     */
    @Value("${spring.datasource.connectionTimeout}")
    private int connectionTimeout;

    /**
     * maxLifetime hikari property
     */
    @Value("${spring.datasource.maxLifetime}")
    private int maxLifetime;

    /**
     * maximumPoolSize hikari property
     */
    @Value("${spring.datasource.maximumPoolSize}")
    private int maximumPoolSize;

    /**
     * minimumIdle hikari property
     */
    @Value("${spring.datasource.minimumIdle}")
    private int minimumIdle;

    /**
     * idleTimeout hikari property
     */
    @Value("${spring.datasource.idleTimeout}")
    private int idleTimeout;

    /**
     * connectionTestQuery hikari property
     */
    @Value("${spring.datasource.connectionTestQuery}")
    private String connectionTestQuery;

    /**
     * Datasource configuration
     *
     * @return HikariDataSource connection pool configured
     */
    @Bean
    public DataSource primaryDataSource() {

        Properties dataSourceProperties = new Properties();
        dataSourceProperties.put("user", user);
        dataSourceProperties.put("password", password);

        Properties hikariProperties = new Properties();
        hikariProperties.put("jdbcUrl", dataSourceUrl);
        hikariProperties.put("poolName", poolName);
        hikariProperties.put("maximumPoolSize", maximumPoolSize);
        hikariProperties.put("minimumIdle", minimumIdle);
        hikariProperties.put("connectionTimeout", connectionTimeout);
        hikariProperties.put("connectionTestQuery", connectionTestQuery);
        hikariProperties.put("idleTimeout", idleTimeout);
        hikariProperties.put("dataSourceProperties", dataSourceProperties);

        return new HikariDataSource(new HikariConfig(hikariProperties));
    }
}