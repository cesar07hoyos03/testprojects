package com.example.springredis;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Coffee {
    private String id;
    private String name;
}